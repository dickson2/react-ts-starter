const { REACT_APP_API_SERVER } = process.env

export async function fetchLogin(username: string, password: string) {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/users/login`, {
        method: "POST",
        headers : {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            username,
            password
        })
    })
    return res;
}

export async function fetchFacebookLogin(accessToken: string) {
    console.log(accessToken)
    const res = await fetch(`${REACT_APP_API_SERVER}/api/users/loginFacebook`, {
        method: "POST",
        headers : {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            accessToken
        })
    })
    return res;
}