import { Dispatch } from "redux";
import { fetchLogin, fetchFacebookLogin } from "../../api/auth"
import { IAuthAction, loginSuccess, loadToken } from "./actions";
import {
    CallHistoryMethodAction, push
} from 'connected-react-router';
export function loginThunk(username: string, password: string) {
    return async (dispatch: Dispatch<IAuthAction | CallHistoryMethodAction>) => {
        const res = await fetchLogin(username, password)
        const result = await res.json()
        if (res.ok) {
            localStorage.setItem('token', result)
            dispatch(loginSuccess())
            dispatch(loadToken(result))
            dispatch(push('/todos'))
        }
    }
}

export function loginFacebookThunk(accessToken: string) {
    return async (dispatch: Dispatch<IAuthAction | CallHistoryMethodAction>) => {
        const res = await fetchFacebookLogin(accessToken)
        const result = await res.json()
        if (res.ok) {
            console.log(result)
            localStorage.setItem('token', result)
            dispatch(loginSuccess())
            dispatch(loadToken(result))
            dispatch(push('/todos'))
        }
    }
}
