import { createStore, combineReducers, Action, applyMiddleware, compose } from 'redux';
import thunk, { ThunkDispatch } from 'redux-thunk';
import logger from 'redux-logger';
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction,
} from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { IAuthState } from './auth/state';
import { IAuthAction } from './auth/actions';
import { authReducers } from './auth/reducers';
export const history = createBrowserHistory();

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

export interface IRootState {
  router: RouterState;
  auth: IAuthState;
}

type IRootAction = CallHistoryMethodAction | IAuthAction;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers<IRootState>({
  router: connectRouter(history),
  auth: authReducers,
});
const store = createStore<IRootState, Action<any>, {}, {}>(
  rootReducer,
  composeEnhancers(
    applyMiddleware(logger),
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
  )
);
export default store;
