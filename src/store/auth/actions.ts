export function test() {
    return {
        type: "TEST" as const
    }
}

export function loadToken(token: string) {
    return {
        type: "@@Auth/LOAD_TOKEN" as const,
        token
    }
}

export function loginSuccess() {
    return {
        type: "@@Auth/LOGIN" as const
    }
}

export type IAuthAction =
    | ReturnType<typeof test>
    | ReturnType<typeof loginSuccess>
    | ReturnType<typeof loadToken>