import { IAuthState } from "./state"
import { IAuthAction } from "./actions"
import jwt from 'jwt-decode'
import { JWTPayload } from "./state"
const initialState: IAuthState = {
    isAuthenticate: (localStorage.getItem('token') !=null),
    msg: "",
    user: {
        username: loadToken()
    }
}

function loadToken() {
    const token = localStorage.getItem('token')
    if (token) {
        const payload: JWTPayload = jwt(token)
        return payload.username
    }
    return ""
}

export const authReducers = (state: IAuthState = initialState, action: IAuthAction): IAuthState => {
    switch (action.type) {
        case "@@Auth/LOGIN":
            return {
                ...state,
                isAuthenticate: true
            } 
        case "@@Auth/LOAD_TOKEN":
            const payload: JWTPayload = jwt(action.token)
            const {username} = payload
            return {
                ...state,
                user: {
                    username: username
                }
            }
        default : 
            return state
    }
}