export interface IAuthState{
    isAuthenticate: boolean
    msg: string
    user: JWTPayload
}

export interface JWTPayload {
    username: string,
}